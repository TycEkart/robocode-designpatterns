package designpatterns;

/**
 * MyClass - a class by (your name here)
 */
public class Vector2
{
	public static final Vector2 Y_AXIS = new Vector2(0,1);
	private double x;
	private double y;
	
	public double X(){
		return x;
	}
	public double Y(){
		return y;
	}

	public Vector2 (double x, double y){
		this.x = x;
		this.y = y;
	}

	public static double distance(Vector2 a, Vector2 b){
		return Math.sqrt(Math.pow(b.x-a.x,2) + Math.pow(b.y-a.y,2));
	}
	
	public Vector2 add(Vector2 v){
		v.x+=x;
		v.y+=y;
		return v;
	}
	
	public Vector2 min(Vector2 v){
		Vector2 n = new Vector2(this.x-v.x, this.y-v.y);
	return n;
	}
	public double magnitude(){
		return Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
	}
	
	public static double getDot(Vector2 a, Vector2 b){
		return (a.x*b.x) + (a.y*b.y);
	}
	
	public static double getAngle(Vector2 a, Vector2 b){
		return Math.acos(getDot(a, b) / (a.magnitude()*b.magnitude()));
	}
	
	public static int orientation(Vector2 a, Vector2 b){
		double z =a.x * b.y  +  a.y + b.y;
		return devideAbs(z);
	}
	
	public static int devideAbs(double a){
		if(a==0){
			return 1;
		}else{
			return  Math.abs((int)a)/(int)a;
		}
	}

	public double toPolarAngle (){
	//	double angle = getAngleD(this, Y_AXIS);
		double angle = Math.toDegrees(Math.atan2(x,y));
		//polar
		angle =(360 + angle )%360;
		//System.out.println("polar : " + angle);	
		
		
		return angle;
	}

	public static double getAngleD(Vector2 a, Vector2 b){
		return Math.toDegrees(getAngle(a, b));
	}
	public String toString(){
		return "{"+ x + ", " + y +"}";
	}
	
	public static Vector2 PolarToNormal(double polarAngle){
		return new Vector2(Math.sin(polarAngle), Math.cos(polarAngle));
	}
}
