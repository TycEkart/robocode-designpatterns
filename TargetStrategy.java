package designpatterns;
import robocode.*;
import java.util.*;

/**
 * MyClass - a class by (your name here)
 */
public interface TargetStrategy
{
	 Target evaluateTarget(Robot r, List<Target> targets); 
}
