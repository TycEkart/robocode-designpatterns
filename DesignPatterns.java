package designpatterns;
import robocode.*;
import java.awt.Graphics2D;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * DesignPatterns - a robot by (your name here)
 */
public class DesignPatterns extends Robot
{
	private RobotContext context;
	private RobotState offenceState = new OffenceState();
	private RobotState defenseState = new DefenseState();
	private RobotState scanningState = new ScanningState();
	private int hitCount = 0;

	
	/**
	 * run: DesignPatterns's default behavior
	 */
	public void run() {
		context = new RobotContext(this);
			
		//Start in offence state
		context.setState(scanningState);
		
		// Robot main loop
		while(true) {
			if(!context.run()){
				context.setState(evaluate());
			}
		}
	}

	public RobotState evaluate(){
		if(SingletonTargetManager.getInstance().getCurrentTarget()!=null){
			return offenceState;
		}
			return scanningState;
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e)
	{
		context.onScannedRobot(e);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		if(++hitCount >=3){
			context.setState(this.defenseState);
			hitCount=0;
		}
		context.onHitByBullet(e);
	}
	
	public void onHitWall(HitWallEvent event) {
		context.onHitWall(event);
	}
	
	 public void onPaint(Graphics2D g) {
		context.onPaint(g);
	 }
	 
	public void	onRoundEnded(){
		context.onRoundEnded();
	}
	
	public static Vector2 calculateTarget(Robot robot, double bearing, double distance){
		double angle = Math.toRadians((robot.getHeading() + bearing)%360);
		return new Vector2(robot.getX() + Math.sin(angle) * distance, robot.getY() + Math.cos(angle) * distance);
	}
	public static double calculateDeltaAngle(Vector2 pos, Vector2 t, double angle){
		if(t!=null){
			return deltaAngles(angle, t.min(pos).toPolarAngle());
		}else{
			return 0;
		}
	}
	
	public static double deltaAngles(double a, double b){
		double delta= b - a;
		delta%=360;
		if(delta>180){
			delta-=360;
		}
		if(delta<-180){
			delta+=360;
		}
		return delta;
	}
}
