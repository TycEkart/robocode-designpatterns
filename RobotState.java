package designpatterns;
import robocode.*;
import java.awt.Color;
import java.awt.Graphics2D;
/**
 * MyClass - a class by (your name here)
 */
public interface RobotState
{
	boolean handleRun(Robot robot);
	void onStateEnter(Robot robot);
	void onStateLeft(Robot robot);
	void onHitWall(Robot robot, HitWallEvent event);
	void handleScannedRobot(Robot robot, ScannedRobotEvent e);
	void handleHitByBullet(Robot robot, HitByBulletEvent e);
	Color getColor();
	void onPaint(Robot r, Graphics2D g);
	void onRoundEnded();
}
