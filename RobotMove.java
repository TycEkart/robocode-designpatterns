package designpatterns;
import robocode.*;
/**
 * MyClass - a class by (your name here)
 */
public class RobotMove extends RobotCommandStrategy
{

	public RobotMove(IRobotStrategy s, Robot r){
		super(s,r);
	}
	public void execute(){
		strategy.momvementBot(robot);
	}
}
