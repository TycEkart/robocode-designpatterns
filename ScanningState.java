package designpatterns;
import robocode.*;
import java.awt.Color;
import java.awt.Graphics2D;
/**
 * MyClass - a class by (your name here)
 */
public class ScanningState implements RobotState
{

	public boolean handleRun(Robot robot)
	{
		robot.turnRadarRight(360);
		return SingletonTargetManager.getInstance().getCurrentTarget() == null;
	}
	
	public Color getColor()
	{
		return Color.white;
	}
	
	public void handleScannedRobot(Robot robot, ScannedRobotEvent e)
	{
		SingletonTargetManager.getInstance().setCurrentTarget(DesignPatterns.calculateTarget(robot, e.getBearing(), e.getDistance()), e.getName());
	}
	
	public void handleHitByBullet(Robot robot, HitByBulletEvent e)
	{
	}
	
	public void onStateEnter(Robot robot)
	{
		robot.setAdjustRadarForRobotTurn(true);	
	}
	
	public void onStateLeft(Robot robot)
	{
			robot.turnRadarLeft(robot.getHeading()-robot.getRadarHeading());	
			robot.setAdjustRadarForRobotTurn(false);	
	}
	
	
	public void onHitWall(Robot robot, HitWallEvent event) 
	{
	}
	public void onPaint(Robot r, Graphics2D g) {
 		return;
	}
	public void	onRoundEnded(){
	}	
}
