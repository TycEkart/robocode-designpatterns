package designpatterns;
import robocode.*;
/**
 * MyClass - a class by (your name here)
 */
public class RobotTurnBot extends RobotCommandStrategy
{
	private double angle;

	public RobotTurnBot(IRobotStrategy s, Robot r, double a){
		super(s,r);
		angle = a;
	}
	public void execute(){
		strategy.turnRobot(robot, angle);
	}
}


