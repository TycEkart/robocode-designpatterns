package designpatterns;

/**
 * MyClass - a class by (your name here)
 */
public class Target
{
	public String name;
	public Vector2 position;
	
	public Target(Vector2 position, String name){
		this.position = position;
		this.name = name;
	}
}
