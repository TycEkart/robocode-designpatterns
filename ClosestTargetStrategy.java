package designpatterns;
import robocode.*;
import java.util.*;
/**
 * MyClass - a class by (your name here)
 */
public class ClosestTargetStrategy implements TargetStrategy
{
	public Target evaluateTarget(Robot r, List<Target> targets){
		if(targets!=null && !targets.isEmpty()){
			Target closeTarget = targets.get(0);
			double closeD = 1000000;
			Vector2 p = new Vector2(r.getX(), r.getY());
			for (Target t : targets){
					double d = Vector2.distance(p, t.position);
				if(closeD < d){
					closeD = d;
					closeTarget = t;
				}
			}
			return closeTarget;
		}
		return null;
	}
}
