package designpatterns;
import robocode.*;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 * MyClass - a class by (your name here)
 */
public class RobotContext
{
	private RobotState state;
	private DesignPatterns robot;
	private RobotState swapState;

	public RobotContext(DesignPatterns robot){
		this.robot = robot;
		state = new OffenceState();
	}
	
	public void setState(RobotState state)
	{
		if(!this.state.equals(state)){
			this.swapState = state;
		}
	}
	
	public RobotState getState()
	{
		return this.state;
	}

	public boolean run(){
		if(swapState!=null && swapState != state){
			swapState.onStateLeft(robot);
			state = swapState;
			swapState = null;
			state.onStateEnter(robot);
			robot.setColors(this.state.getColor(),Color.blue,Color.green); // body,gun,radar
		}
		return state.handleRun(robot);
	}
	
	public void onScannedRobot(ScannedRobotEvent e)
	{
		state.handleScannedRobot(robot, e);
	}
	
	public void onHitByBullet(HitByBulletEvent e) {
		state.handleHitByBullet(robot, e);
	}
	
	public void onHitWall(HitWallEvent event) {
		state.onHitWall(robot, event);
	}
	 public void onPaint(Graphics2D g) {
		state.onPaint(robot, g);
	 }
	 
	public void	onRoundEnded(){
		state.onRoundEnded();
	}

	
}
