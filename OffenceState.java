package designpatterns;
import robocode.*;
import java.awt.Color;
import java.awt.Graphics2D;
/**
 * MyClass - a class by (your name here)
 */
public class OffenceState implements RobotState
{
	private IRobotStrategy strategy = new ConcreteStrategyStop();
	private	int resetTargetTick=0;
	
	public boolean handleRun(Robot robot)
	{
		SingletonTargetManager.getInstance().evaluateTargets(robot, new ClosestTargetStrategy());
		Vector2 target = SingletonTargetManager.getInstance().getCurrentTarget();
		strategy.turnGun(robot, target);
		if(target!=null){
			robot.fire(1);
		}
		strategy.turnRadar(robot, target);

		strategy.momvementBot(robot);
		if(resetTargetTick++ > 0){
			System.out.println("reset, lost targets") ;
			SingletonTargetManager.getInstance().resetTargets();
			resetTargetTick = 0;
		}	
		return true;
	}
	
	public Color getColor()
	{
		return Color.red;
	}
	
	public void handleScannedRobot(Robot robot, ScannedRobotEvent e)
	{
		resetTargetTick = 0;
		SingletonTargetManager.getInstance().updateTargets(DesignPatterns.calculateTarget(robot, e.getBearing(), e.getDistance()), e.getName());
		//strategy.turnRadar(robot, target);
	}
	
	public void handleHitByBullet(Robot robot, HitByBulletEvent e)
	{
		strategy = new ConcreteStrategyDrive();
	}
	
	public void onStateEnter(Robot robot){
		resetTargetTick = 0;
		strategy = new ConcreteStrategyStop();
		robot.setAdjustRadarForRobotTurn(true);	
		robot.setAdjustRadarForGunTurn(true);
		robot.setAdjustGunForRobotTurn(true) ;
	}
	public void onStateLeft(Robot robot){
		robot.setAdjustRadarForRobotTurn(false);	
		robot.setAdjustGunForRobotTurn(false) ;
		robot.setAdjustRadarForGunTurn(false);
	}
	
	
	public void onHitWall(Robot robot, HitWallEvent event) 
	{
		double delta = (robot.getHeading()-event.getBearing())%180;
		strategy.turnRobot(robot, delta);
	}
	 public void onPaint(Robot r, Graphics2D g) {
	     // Set the paint color to red
	 	g.setColor(java.awt.Color.YELLOW);
	     // Paint a filled rectangle at (50,50) at size 100x150 pixels
		 Vector2 target = SingletonTargetManager.getInstance().getCurrentTarget();
		 if(target !=null){
		     g.drawLine((int)r.getX(), 
						(int)r.getY(),
						(int) target.X(),
						(int) target.Y());
		}		
 		g.setColor(java.awt.Color.RED);
	     // Paint a filled rectangle at (50,50) at size 100x150 pixels
		 double angle =r.getGunHeading();
	     g.drawLine((int)r.getX(), 
					(int)r.getY(),
					(int)(r.getX() + Math.sin(Math.toRadians(angle)) * 100),
					(int)(r.getY() + Math.cos(Math.toRadians(angle)) * 100));
		 g.setColor(java.awt.Color.GREEN);
	     // Paint a filled rectangle at (50,50) at size 100x150 pixels
		 angle = r.getRadarHeading();
	     g.drawLine((int)r.getX(), 
					(int)r.getY(),
					(int)(r.getX() + Math.sin(Math.toRadians(angle)) * 100),
					(int)(r.getY() + Math.cos(Math.toRadians(angle)) * 100));

	 }
	 
		public void	onRoundEnded(){
		}
	}

