package designpatterns;
import robocode.*;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.*;
/**
 * MyClass - a class by (your name here)
 */
public class DefenseState implements RobotState
{
	private  List<Command> toDoCommands;
	private  IRobotStrategy strategy = new ConcreteStrategyKeepDistance();
	private long changeOn = -1;	
	
	public boolean handleRun(Robot robot)
	{
		if(!toDoCommands.isEmpty()){
			handleCommands();
		}
		toDoCommands.add(new RobotMove(strategy, robot));
		
		if(changeOn != -1 && robot.getTime() < changeOn){
			return true;
		}
		return false;
	}
	
	private void handleCommands(){
		List<Command> commands = new ArrayList<Command>(toDoCommands);
		System.out.println("todo: " + commands.size());
		for(Command c : commands){
			System.out.println("todo: " + toDoCommands.size());
			c.execute();
			toDoCommands.remove(c);
			System.out.println("removed " + toDoCommands.size());
		}
		System.out.println("endCommands " + toDoCommands.size());
	}
	
	public boolean shouldBreak(){
		return false;
	}
	
	public Color getColor()
	{
		return Color.green;
	}
	
	public void handleScannedRobot(Robot robot, ScannedRobotEvent e)
	{
		//toDoCommands.add(new RobotTurnGun(strategy, robot, DesignPatterns.calculateTarget(robot, e.getBearing(), e.getDistance())));
	}
	
	public void handleHitByBullet(Robot robot, HitByBulletEvent e)
	{	
		toDoCommands.add(new RobotTurnBot(strategy, robot, 90 - e.getBearing()));
	}
	
	public void onStateEnter(Robot robot)
	{
		toDoCommands = new ArrayList<Command>();
		toDoCommands.add(new RobotMove(strategy, robot));
		changeOn = robot.getTime() + 50;
	}
	
	public void onStateLeft(Robot robot)
	{
		changeOn = -1;
	}
	
	public void onHitWall(Robot robot, HitWallEvent event) 
	{
		toDoCommands.add(new RobotTurnBot(strategy, robot, 90 - event.getBearing()));
		toDoCommands.add(new RobotMove(strategy, robot));
	}
	public void onPaint(Robot r, Graphics2D g) {
			return;
 	}
	
	public void	onRoundEnded(){
	}
}
