package designpatterns;
import robocode.*;
import java.util.*;

/**
 * MyClass - a class by (your name here)
 */
public class SingletonTargetManager
{
	private static final SingletonTargetManager _instance = new SingletonTargetManager();
	private Target currentTarget;
	private List<Target> targets = new ArrayList<Target>();
	private SingletonTargetManager(){

	}
	public static SingletonTargetManager getInstance(){
		return _instance;
	}
	
	public Vector2 getCurrentTarget(){
		if(currentTarget == null){
			return null;	
		}
		return currentTarget.position;
	}
		
	public void setCurrentTarget(Vector2 newTarget, String name){
		this.currentTarget = new Target(newTarget, name);
		updateTargets(newTarget, name);
	}
	
	public void updateTargets(Vector2 position, String name){
		for (Target t : targets){
			if(t.name.equals(name)){
				t.position = position;
				return;
			}
		}
		targets.add(new Target(position, name));
	}
	public void evaluateTargets(Robot r, TargetStrategy ts){
		currentTarget =	ts.evaluateTarget(r, targets);
	}
	public void resetTargets(){
		currentTarget = null;
		targets = new ArrayList<Target>();
	}
}
