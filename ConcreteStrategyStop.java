package designpatterns;
import robocode.*;
/**
 * MyClass - a class by (your name here)
 */
public class ConcreteStrategyStop implements IRobotStrategy
{

	public void momvementBot(Robot r){
	}
	public void turnRobot(Robot r, double targetDegrees){
	}	
	public void turnGun(Robot r, Vector2 t){
		if(t!=null){
			r.turnGunRight(DesignPatterns.calculateDeltaAngle(new Vector2(r.getX(), r.getY()), t, r.getGunHeading()));
		}
	}
	public void turnRadar(Robot r, Vector2 t){
		if(t!=null){
			r.turnRadarRight(DesignPatterns.calculateDeltaAngle(new Vector2(r.getX(), r.getY()), t, r.getRadarHeading()));
		}else{
			r.turnRadarRight(DesignPatterns.deltaAngles(r.getGunHeading(), r.getRadarHeading()) + 360);
		}
	}	
}
