package designpatterns;
import robocode.*;
/**
 * MyClass - a class by (your name here)
 */
public class RobotTurnGun  extends RobotCommandStrategy
{	
	private Vector2 target;

	public RobotTurnGun(IRobotStrategy s, Robot r, Vector2 t){
		super(s,r);
		target =t;
	}
	public void execute(){
		strategy.turnGun(robot, target);
	}
}
