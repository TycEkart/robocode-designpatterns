package designpatterns;
import robocode.*;

/**
 * MyClass - a class by (your name here)
 */
public interface IRobotStrategy
{
	void momvementBot(Robot r);
	void turnRobot(Robot r, double targetDegrees);
	void turnGun(Robot r, Vector2 t);
	void turnRadar(Robot r, Vector2 t);
}
