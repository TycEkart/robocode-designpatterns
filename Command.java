package designpatterns;

/**
 * MyClass - a class by (your name here)
 */
public interface Command
{
	void execute(); 
}
