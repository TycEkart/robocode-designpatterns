package designpatterns;
import robocode.*;

public abstract class RobotCommandStrategy implements Command
{
	protected Robot robot; 
	protected IRobotStrategy strategy;

	public RobotCommandStrategy(IRobotStrategy s, Robot r){
		robot = r;
		strategy = s;
	}
	public abstract void execute();
}


